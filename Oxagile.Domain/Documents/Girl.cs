﻿using MongoDB.Bson.Serialization.Attributes;

namespace Oxagile.Domain.Documents
{
    public class GirlDocument
    {
        [BsonId]
        public string Id { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
        public string ImageUrl { get; set; }
        public BodyType Body { get; set; }
    }

    public class BodyType
    {
        public BodyType()
        {
        }

        public BodyType(int chestSize, int waistLineSize, int hipSize)
        {
            ChestSize = chestSize;
            WaistLineSize = waistLineSize;
            HipSize = hipSize;
        }

        public int ChestSize { get; set; }
        public int WaistLineSize { get; set; }
        public int HipSize { get; set; }
    }
}
