﻿using System;
using System.Collections.Generic;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using Oxagile.Domain.Documents;
using Oxagile.Infrastructure.Mvc;

namespace Oxagile.Infrastructure.Services
{
    public class GirlsService : ViewServiceFiltered<GirlDocument, GirlsFilter>
    {
        public GirlsService(MongoDocumentsDatabase database)
            : base(database)
        {

        }

        protected override MongoCollection<GirlDocument> Items
        {
            get { return Database.MyPrettyGirls; }
        }

        protected override IEnumerable<IMongoQuery> BuildFilterQuery(GirlsFilter filter)
        {
            if (filter.Age!= null)
            {
                yield return Query<GirlDocument>.EQ(x => x.Age, filter.Age);
            }
            if (filter.ChestSize != null)
            {
                yield return Query<GirlDocument>.EQ(x => x.Body.ChestSize, filter.ChestSize);
            }

            if (filter.HipSize != null)
            {
                yield return Query<GirlDocument>.EQ(x => x.Body.HipSize, filter.HipSize);
            }
            if (filter.WaistLineSize != null)
            {
                yield return Query<GirlDocument>.EQ(x => x.Body.WaistLineSize, filter.WaistLineSize);
            }

            if (!string.IsNullOrEmpty(filter.Name))
            {
                yield return Query<GirlDocument>.EQ(x => x.Name, filter.Name);
            }
        }
    }

    public class GirlsFilter : BaseFilter
    {
        public int? ChestSize { get; set; }
        public int? WaistLineSize { get; set; }
        public int? HipSize { get; set; }
        public int? Age { get; set; }
        public string Name { get; set; }
    }
}
