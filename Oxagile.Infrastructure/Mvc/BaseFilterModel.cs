﻿using System.Collections.Generic;

namespace Oxagile.Infrastructure.Mvc
{
    public class BaseFilterModel<T> where T : BaseFilter, new()
    {
        public string OrderByKey { get; set; }
        public bool Desc { get; set; }
        public int CurrentPage { get; set; }
        public int TotalPages { get; set; }
        public int ItemsPerPage { get; set; }

        public virtual IEnumerable<string> DoNotReturnPropertiesNames
        {
            get { return new[] { "DoNotReturnPropertiesNames", "TotalPages" }; }
        }

        public BaseFilterModel()
        {
            CurrentPage = 1;
            TotalPages = 1;
            ItemsPerPage = 50;
        }

        public virtual T ToFilter()
        {
            var filter = new T
            {
                PagingInfo = new PagingInfo { CurrentPage = CurrentPage, ItemsPerPage = ItemsPerPage }
            };
            if (!string.IsNullOrEmpty(OrderByKey))
            {
                filter.AddOrder(OrderByKey, Desc);
            }
            return filter;
        }

        public void UpdatePagingInfo(PagingInfo pagingInfo)
        {
            CurrentPage = pagingInfo.CurrentPage;
            TotalPages = pagingInfo.TotalPagesCount;
        }
    }
}
