﻿using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Bson.Serialization.Options;
using Oxagile.Infrastructure;
using Oxagile.Infrastructure.Settings;
using StructureMap;

namespace Oxagile.UI
{
    public class Bootstaper
    {
        public void Configure(IContainer container)
        {
            ConfigureSettings(container);
            ConfigureMongoDb(container);
            ConfigureServices(container);
        }

        private void ConfigureServices(IContainer container)
        {
        }

        public void ConfigureSettings(IContainer container)
        {
            var settings = SettingsMapper.Map<OxagileSettings>();

            container.Configure(config => config.For<OxagileSettings>().Singleton().Use(settings));
        }

        public void ConfigureMongoDb(IContainer container)
        {
            var myConventions = new ConventionProfile();
            myConventions.SetIgnoreExtraElementsConvention(new AlwaysIgnoreExtraElementsConvention());
            BsonClassMap.RegisterConventions(myConventions, type => true);
            DateTimeSerializationOptions.Defaults = DateTimeSerializationOptions.LocalInstance;

            RegisterBsonMaps();

            var settings = container.GetInstance<OxagileSettings>();
            container.Configure(
                config =>
                    config.For<MongoDocumentsDatabase>()
                        .Singleton()
                        .Use(new MongoDocumentsDatabase(settings.MongoConnectionString)));
        }

        private static void RegisterBsonMaps()
        {
        }
    }
}