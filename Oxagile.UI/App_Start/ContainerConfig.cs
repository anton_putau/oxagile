﻿using System.Web.Mvc;
using Oxagile.Infrastructure.Services;
using Oxagile.UI.DependencyResolution;
using StructureMap;

namespace Oxagile.UI
{
    public static class ContainerConfig
    {
        public static void Configure()
        {
            ObjectFactory.Initialize(x => x.Scan(scan =>
            {
                scan.TheCallingAssembly();
                scan.WithDefaultConventions();
            }));

            IContainer container = ObjectFactory.Container;
            var resolver = new StructureMapDependencyResolver(container);
            container.Configure(config =>
            {
                config.For<GirlsService>().Singleton().Use<GirlsService>();  
            });

            //Domain
            new Bootstaper().Configure(container);
            //MVC
            DependencyResolver.SetResolver(resolver);
        }
    }
}