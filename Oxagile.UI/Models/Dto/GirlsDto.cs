﻿using System.Collections.Generic;
using Oxagile.Domain.Documents;

namespace Oxagile.UI.Models.Dto
{
    public class GirlsDto
    {
        public GirlsFilterViewModel Filter { get; set; }
        public List<GirlDocument> Items { get; set; }
    }
}