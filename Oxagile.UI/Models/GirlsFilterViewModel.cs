﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Oxagile.Infrastructure.Mvc;
using Oxagile.Infrastructure.Services;

namespace Oxagile.UI.Models
{
    public class GirlsFilterViewModel : BaseFilterModel<GirlsFilter>
    {
        public int? ChestSize { get; set; }
        public int? WaistLineSize { get; set; }
        public int? HipSize { get; set; }
        public int? Age { get; set; }
        public string Name { get; set; }

        public override GirlsFilter ToFilter()
        {
            var filter = base.ToFilter();

            filter.Name = Name;
            filter.Age = Age;
            filter.HipSize = HipSize;
            filter.ChestSize = ChestSize;
            filter.WaistLineSize = WaistLineSize;

            return filter;
        }
    }
}