﻿using Oxagile.Infrastructure.Settings;

namespace Oxagile.UI
{
    public class OxagileSettings
    {
        [SettingsProperty("mongo.connection_string")]
        public string MongoConnectionString { get; set; }
    }
}