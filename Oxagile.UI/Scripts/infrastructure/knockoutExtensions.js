﻿ko.utils.buildQueryUrl = function (data) {
    var query = "";
    parseValues(data, function (key, value) {
        query += encodeURIComponent(key) + "=" + encodeURIComponent(value) + "&";
    });

    return query;
};

var parseValues = function (data, callback, prefix) {
    prefix = prefix || "";
    for (var key in data) {
        if (data[key] == null) {
            continue;
        }
        if (typeof data[key] == "object") {
            parseValues(data[key], callback, prefix + key + ".");
        } else {
            callback(prefix + key, ko.utils.unwrapObservable(data[key]));
        }
    }
};