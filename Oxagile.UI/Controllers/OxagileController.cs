﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AttributeRouting;
using AttributeRouting.Web.Mvc;
using MongoDB.Bson;
using Oxagile.Domain.Documents;
using Oxagile.Infrastructure.Services;
using Oxagile.UI.Models;
using Oxagile.UI.Models.Dto;

namespace Oxagile.UI.Controllers
{
    [RoutePrefix("oxagile")]
    public class OxagileController : Controller
    {
        private readonly GirlsService _girlsService;

        public OxagileController(GirlsService girlsService)
        {
            _girlsService = girlsService;
        }

        //
        // GET: /Oxagile/
        [GET("WelcomePage")]
        public ActionResult Index()
        {
            return View();
        }

        [GET("PageWithFiltering")]
        public ActionResult PageWithFiltering()
        {
            return View(new GirlsFilterViewModel
            {
                ItemsPerPage = 50
            });
        }

        [GET("GirlsList")]
        public ActionResult GirlsList(GirlsFilterViewModel filterModel)
        {
            var filter = filterModel.ToFilter();

            var result = _girlsService.GetByFilter(filter).ToList();
            filterModel.UpdatePagingInfo(filter.PagingInfo);
            var model = new GirlsDto
            {
                Filter = filterModel,
                Items = result
            };
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [GET("development")]
        public ActionResult Development()
        {
            var r = new Random();
            for (int i = 0; i < 100; i++)
            {
                var age = r.Next(18,28);
                var name = "Lisa" + i;

                var c = 90 + r.Next(-10,10);
                var w = 60 +  r.Next(-10,10);
                var h = 90 +  r.Next(-10,10);

                var bodyType = new BodyType(c, w, h);

                _girlsService.Save(new GirlDocument
                {
                    Age = age, 
                    Name = name,
                    Body = bodyType,
                    ImageUrl = "/emma_watson.jpg",
                    Id = ObjectId.GenerateNewId().ToString()
                });
            }
            return RedirectToAction("GirlsList");
        }

    }
}
